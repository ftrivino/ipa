From de6f074538f6641fd9d84bed204a3d4d50eccbe5 Mon Sep 17 00:00:00 2001
From: Rob Crittenden <rcritten@redhat.com>
Date: Thu, 4 Aug 2022 12:04:41 -0400
Subject: [PATCH] Set default on group pwpolicy with no grace limit in upgrade

If an existing group policy lacks a password grace limit
update it to -1 on upgrade.

Fixes: https://pagure.io/freeipa/issue/9212

Signed-off-by: Rob Crittenden <rcritten@redhat.com>
Reviewed-By: Florence Blanc-Renaud <frenaud@redhat.com>
---
 .../updates/90-post_upgrade_plugins.update    |  1 +
 ipaserver/install/plugins/update_pwpolicy.py  | 66 +++++++++++++++++++
 2 files changed, 67 insertions(+)

diff --git a/install/updates/90-post_upgrade_plugins.update b/install/updates/90-post_upgrade_plugins.update
index c7ec71d492b0ac0e7641d586b7e7fa7501743bc2..6fe91aa6c6310a69a7f0feb1ad62243945db67f9 100644
--- a/install/updates/90-post_upgrade_plugins.update
+++ b/install/updates/90-post_upgrade_plugins.update
@@ -26,6 +26,7 @@ plugin: update_ra_cert_store
 plugin: update_mapping_Guests_to_nobody
 plugin: fix_kra_people_entry
 plugin: update_pwpolicy
+plugin: update_pwpolicy_grace
 
 # last
 # DNS version 1
diff --git a/ipaserver/install/plugins/update_pwpolicy.py b/ipaserver/install/plugins/update_pwpolicy.py
index dca44ce4369dfc11f83a412a1249bb045d46713f..4185f034313bd49ca68e86c620043af6ead5f6d6 100644
--- a/ipaserver/install/plugins/update_pwpolicy.py
+++ b/ipaserver/install/plugins/update_pwpolicy.py
@@ -78,3 +78,69 @@ class update_pwpolicy(Updater):
                 return False, []
 
         return False, []
+
+
+@register()
+class update_pwpolicy_grace(Updater):
+    """
+    Ensure all group policies have a grace period set.
+    """
+
+    def execute(self, **options):
+        ldap = self.api.Backend.ldap2
+
+        base_dn = DN(('cn', self.api.env.realm), ('cn', 'kerberos'),
+                     self.api.env.basedn)
+        search_filter = (
+            "(&(objectClass=krbpwdpolicy)(!(passwordgracelimit=*)))"
+        )
+
+        while True:
+            # Run the search in loop to avoid issues when LDAP limits are hit
+            # during update
+
+            try:
+                (entries, truncated) = ldap.find_entries(
+                    search_filter, ['objectclass'], base_dn, time_limit=0,
+                    size_limit=0)
+
+            except errors.EmptyResult:
+                logger.debug("update_pwpolicy: no policies without "
+                             "passwordgracelimit set")
+                return False, []
+
+            except errors.ExecutionError as e:
+                logger.error("update_pwpolicy: cannot retrieve list "
+                             "of policies missing passwordgracelimit: %s", e)
+                return False, []
+
+            logger.debug("update_pwpolicy: found %d "
+                         "policies to update, truncated: %s",
+                         len(entries), truncated)
+
+            error = False
+
+            for entry in entries:
+                # Set unlimited BIND by default
+                entry['passwordgracelimit'] = -1
+                try:
+                    ldap.update_entry(entry)
+                except (errors.EmptyModlist, errors.NotFound):
+                    pass
+                except errors.ExecutionError as e:
+                    logger.debug("update_pwpolicy: cannot "
+                                 "update policy: %s", e)
+                    error = True
+
+            if error:
+                # Exit loop to avoid infinite cycles
+                logger.error("update_pwpolicy: error(s) "
+                             "detected during pwpolicy update")
+                return False, []
+
+            elif not truncated:
+                # All affected entries updated, exit the loop
+                logger.debug("update_pwpolicy: all policies updated")
+                return False, []
+
+        return False, []
-- 
2.37.2

